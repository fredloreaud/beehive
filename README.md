# beehive

```plantuml
title Connected beehive

actor "[[http://www.solarelectricityhandbook.com/solar-irradiance.html sun]]" as sun

usecase "[[https://www.amazon.fr/ECO-WORTHY-Monocristallin-100Watts-Hors-r%C3%A9seau-Camping-car/dp/B08KZJ4N26/ solar panel]]" as solar
usecase "[[https://www.amazon.fr/ECO-WORTHY-Monocristallin-100Watts-Hors-r%C3%A9seau-Camping-car/dp/B08KZJ4N26/ charge regulator]]" as regulator
usecase "[[https://www.amazon.fr/DSK-10364-Batterie-plomb-technologie/dp/B072VPPDSB/ battery]]" as bat

usecase "[[https://www.sparkfun.com/products/17506 SparkFun LoRa Thing Plus]]" as lora

usecase "[[https://www.amazon.fr/gp/product/B09JPBSTHX/ load cell amplifier]]" as amplifier
usecase "[[https://www.amazon.fr/gp/product/B09JPBSTHX/ load cell]]" as load

usecase "[[https://www.amazon.fr/AZDelivery-DS18B20-Capteur-temp%C3%A9rature-num%C3%A9rique/dp/B01MZG48OE/ temperature sensor]]" as temperature

usecase "[[https://www.amazon.fr/gp/product/B09MLCD78D antenna]]" as antenna
usecase "[[https://www.thethingsnetwork.org/ Things network]]" as network
usecase "[[https://eu1.cloud.thethings.network/console/applications/idobees Things application]]" as application

sun --- solar : 1-4kWH/m2
solar --- regulator : 100-200W 300/800 Wh/day
regulator --- bat : 12V
regulator --- lora : USB-C 5V / 2A / ?Wh/day

lora --- amplifier : GPIO
amplifier --- load
load --- beehive

lora -- temperature : GPIO / 1wire
temperature --- beehive

lora -- antenna : U.FL / SMA
antenna .. network : LoRa
network -- application
application -- beekeeper
```
